## Steam Download Watcher

This tool will observe the Steam downloads and detect and execute an action, you can select, when all downloads are finished.

## Motivation

This tool was created out of personal interest because Steam does not have this functionality by default and you would have to leave your PC running and check yourself if the downloads are finished.

## Installation

**Important: Since this is a Open Source Project the Executeable is not Signed and Windows will show a Warning at the first launch bacause of that**

There is no Installation required. Just head over to the [**Downloads section**](https://bitbucket.org/HellGate/steam-download-watcher/downloads) and get the newest executable.

## Requirements

Version 1.0+: .Net Framework 4.6.2

Version 0.7: .Net Framework 4.6.1

Version 0.6: .Net Framework 4.6

Version 0.1 - 0.5: .Net Framework 4.0 Client Profile

## Contributors

You can contribute by creating pull requests. I will merge then when they fit the requirements (please use a code formatter like code maid).

## License

This project is under the MIT license. The full license can be found in the project files.

## Changelog

* v1.0.1 - Added Sleep Mode
* v1.0 - Major Release: Added Update checking in About Dialog, GUI changes, bugfixes and other changes
* v0.7.1 - Now waits till a Download starts when enabling
* v0.7 - Reworked Download detection to use Registry, Countdown will now cancel if a new Download has started during it, added "Execute now" to the Countdown Window, new "Close Steam" action, removed Steam Path as it is no longer required with the new method
* v0.6 - Updated to .Net 4.6 / C# 6, App will now close correctly and save the settings when the action was executed, new custom Style
* v0.5 - Added fetch App names checkbox, selected action and arguments are now geting saved, some small UI updates
* v0.4 - Added default InfoAction instead of ShutDown, now prevents Sleeping during watching, allows for changing the path to Steam
* v0.3 - Added Minimize to Tray, added About Dialog
* v0.2 - Added Abort Window, allows Custom Commands
* v0.1 - Initial working version