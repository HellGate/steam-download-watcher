﻿using Microsoft.Win32;
using SteamDownloadWatcher.Classes;
using SteamDownloadWatcher.Classes.DownloadFinishActions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows;
using WForms = System.Windows.Forms;

namespace SteamDownloadWatcher.Windows {

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public partial class MainWindow : Window, INotifyPropertyChanged {
        private const double UpdateTime = 15.0; // In seconds

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Property Changed

        private CountdownWindow _countdownWindow = null;
        private bool _detectedNewDownload = false;

        private ObservableCollection<IDownloadFinishAction> _downloadFinishedActions = new ObservableCollection<IDownloadFinishAction>();

        public ObservableCollection<IDownloadFinishAction> DownloadFinishedActions {
            get { return _downloadFinishedActions; }
            set {
                _downloadFinishedActions = value;
                OnPropertyChanged();
            }
        }

        private AppInfo _downloadingApp = null;

        public AppInfo DownloadingApp {
            get { return _downloadingApp; }
            set {
                _downloadingApp = value;
                OnPropertyChanged();
                Application.Current.Dispatcher.Invoke(DownloadChanged);
            }
        }

        private bool _waitingForDownload = true;

        public bool IsDownloading => DownloadingApp != null;

        private bool _watching;

        public bool IsWatching {
            get { return _watching; }
            set {
                if (value != _watching) {
                    _watching = value;
                    UpdateWatchStatus();
                }
            }
        }

        private WForms.NotifyIcon _notify;
        private WForms.MenuItem _enableItem;

        public MainWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            // Download Finished Actions
            DownloadFinishedActions = new ObservableCollection<IDownloadFinishAction>() {
                new InfoAction(),
                new CloseSteamAction(),
                new ShutDownAction(),
                new LogOutAction(),
                new LockComputerAction(),
                new SleepAction(),
                new HibernateAction(),
                new CustomAction(),
            };

            // Tray Icon
            _notify = new WForms.NotifyIcon() {
                Text = "Steam Download Watcher",
                Icon = new System.Drawing.Icon(Application.GetResourceStream(new Uri("pack://application:,,,/Resources/logo.ico")).Stream),
                ContextMenu = new WForms.ContextMenu(),
                Visible = false
            };
            _notify.DoubleClick += RestoreWindow;
            var restore = new WForms.MenuItem("Restore Window");
            restore.Click += RestoreWindow;
            _notify.ContextMenu.MenuItems.Add(restore);
            _enableItem = new WForms.MenuItem("Click to Enable");
            _enableItem.Click += EnableItem_Click;
            _notify.ContextMenu.MenuItems.Add(_enableItem);

            // Check Timer
            var t = new Timer(TimeSpan.FromSeconds(UpdateTime).TotalMilliseconds);
            t.Elapsed += UpdateDownloads;
            t.Start();
            UpdateDownloads(this, null);
        }

        private void RestoreWindow(object sender, EventArgs e) {
            WindowState = WindowState.Normal;
        }

        private void EnableItem_Click(object sender, EventArgs e) {
            IsWatching = !IsWatching;
        }

        private void ChangeStateButton_Clicked(object sender, RoutedEventArgs e) {
            IsWatching = !IsWatching;
        }

        private void UpdateWatchStatus() {
            string statusText = ((IsWatching) ? "Active" : "Inactive");
            ChangeStateButton.Content = statusText;
            _enableItem.Text = statusText;

            ChangeStateButton.IsChecked = IsWatching;
            _enableItem.Checked = IsWatching;

            if (IsWatching) {
                _waitingForDownload = !IsDownloading;
                if (!NativeMethods.PreventSleep()) {
                    MessageBox.Show("Unable to keep Computer awake. Computer might hibernate during download", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            } else {
                NativeMethods.AllowSleep();
            }
        }

        #region Steam Download Checking

        private void DownloadChanged() {
            if (IsWatching && !_waitingForDownload) {
                if (IsDownloading) {
                    // we are downloading again, close countdown window
                    if (_countdownWindow != null) {
                        _detectedNewDownload = true;
                        _countdownWindow.Close();
                        _countdownWindow = null;
                    }
                } else {
                    // only 1 countdown window
                    if (_countdownWindow == null) {
                        _countdownWindow = new CountdownWindow();
                        bool? res = _countdownWindow.ShowDialog();
                        _countdownWindow = null;
                        if (res == true) {
                            // we want to execute the action
                            IsWatching = false;
                            IDownloadFinishAction action = ActionComboBox.SelectedItem as IDownloadFinishAction;
                            action.Execute(ArgumentTextBox.Text);
                            Application.Current.Shutdown();
                        } else {
                            if (!_detectedNewDownload) {
                                // user canceled the countdown. stop observing
                                IsWatching = false;
                            }
                            // else new download started, just continue watching
                        }
                        _detectedNewDownload = false;
                    }
                }
            }
        }

        private void UpdateDownloads(object sender, ElapsedEventArgs e) {
            RegistryKey apps = SteamInfo.SteamApps;

            // check if old one is still updating
            if (DownloadingApp != null) {
                RegistryKey appkey = apps.OpenSubKey(DownloadingApp.SteamId.ToString());
                if (IsRegistryAppUpdating(appkey)) {
                    // still updating, wait for next cycle
                    return;
                }
            }

            // ckeck for any app that is updating
            foreach (string regAppId in apps.GetSubKeyNames()) {
                if (!uint.TryParse(regAppId, out uint appId))
                    continue;

                RegistryKey appkey = apps.OpenSubKey(regAppId);
                if (IsRegistryAppUpdating(appkey)) {
                    _waitingForDownload = false;
                    DownloadingApp = new AppInfo(appId);
                    return;
                }
            }
            DownloadingApp = null;
        }

        private bool IsRegistryAppUpdating(RegistryKey app) {
            object updatingObj = app.GetValue("Updating", null);
            if (updatingObj != null) {
                return Convert.ToBoolean(updatingObj);
            }
            return false;
        }

        #endregion Steam Download Checking

        private void AboutButton_Click(object sender, RoutedEventArgs e) {
            new AboutWindow().ShowDialog();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e) {
            UpdateDownloads(null, null);
        }

        private void FetchNames_Checked(object sender, RoutedEventArgs e) {
            DownloadingApp?.RequestAppName();
        }

        private void Window_StateChanged(object sender, EventArgs e) {
            bool minimized = (WindowState == WindowState.Minimized);
            _notify.Visible = minimized;
            ShowInTaskbar = !minimized;
        }

        protected override void OnClosing(CancelEventArgs e) {
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }
    }
}