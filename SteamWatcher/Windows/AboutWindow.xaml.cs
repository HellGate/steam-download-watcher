﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Xml;

namespace SteamDownloadWatcher.Windows {

    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window {
        private const int RequestTimeout = 30;
        private static readonly string VersionUrl = "https://bitbucket.org/HellGate/steam-download-watcher/downloads/Version.xml";

        private Version _appVersion = null;

        private static bool _checkedVersion = false;
        private static bool _remoteUpdateAviable = false;
        private static Version _remoteVersion = null;
        private static string _remoteDownloadUrl = null;

        public AboutWindow() {
            InitializeComponent();

            _appVersion = Assembly.GetExecutingAssembly().GetName().Version;
            TitleLabel.Content = string.Format((string)TitleLabel.Content, _appVersion.ToString());
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e) {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void CheckUpdate_Click(object sender, RoutedEventArgs e) {
            if (_checkedVersion && _remoteUpdateAviable) {
                Process.Start(new ProcessStartInfo(_remoteDownloadUrl));
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            // Only check once
            if (_checkedVersion) {
                UpdateDowloadButton();
            } else {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (se, ev) => {
                    try {
                        WebRequest request = WebRequest.Create(VersionUrl);
                        request.Timeout = RequestTimeout * 1000;

                        using (WebResponse response = request.GetResponse()) {
                            using (XmlTextReader reader = new XmlTextReader(response.GetResponseStream())) {
                                while (reader.Read()) {
                                    if (reader.IsStartElement()) {
                                        switch (reader.Name) {
                                            case "version":
                                                _remoteVersion = new Version(reader.ReadString());
                                                break;

                                            case "url":
                                                _remoteDownloadUrl = reader.ReadString();
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception ex) {
                        Debug.WriteLine(ex.ToString());
                    }

                    _remoteUpdateAviable = _remoteVersion > _appVersion;
                    _checkedVersion = true;

                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                        UpdateDowloadButton();
                    }));
                };

                worker.RunWorkerAsync();
            }
        }

        private void UpdateDowloadButton() {
            if (_remoteVersion != null) {
                if (_remoteUpdateAviable) {
                    UpdateButton.Content = $"Update to v{_remoteVersion}";
                    UpdateButton.ToolTip = "Downloads the newest Version on your Browser";
                    UpdateButton.Style = Application.Current.Resources["GreenButton"] as Style;
                    UpdateButton.IsEnabled = true;
                } else {
                    UpdateButton.Content = "Already up-to-date";
                }
            } else {
                UpdateButton.Content = "Update Check Failed";
            }
        }
    }
}