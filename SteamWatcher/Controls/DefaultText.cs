﻿using System.Windows.Controls;

namespace SteamDownloadWatcher.Controls {

    /// <summary>
    /// Dummy Class to prevent overwriting the Style of every element that uses a TextBlock
    /// </summary>
    public class DefaultText : TextBlock {
    }
}