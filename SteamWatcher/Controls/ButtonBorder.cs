﻿using System.Windows;
using System.Windows.Media;

namespace SteamDownloadWatcher.Controls {

    public class ButtonBorder {

        public static Brush GetBorder(DependencyObject obj) {
            return (Brush)obj.GetValue(BrushProperty);
        }

        public static void SetBorder(DependencyObject obj, Brush value) {
            obj.SetValue(BrushProperty, value);
        }

        public static readonly DependencyProperty BrushProperty =
            DependencyProperty.RegisterAttached(
                "Border",
                typeof(Brush),
                typeof(ButtonBorder),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Inherits)
            );
    }
}