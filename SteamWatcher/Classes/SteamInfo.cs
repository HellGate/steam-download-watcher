﻿using Microsoft.Win32;
using System;
using System.IO;

namespace SteamDownloadWatcher.Classes {

    public static class SteamInfo {
        public static RegistryKey SteamApps { get; private set; }
        public static string SteamPath { get; private set; }
        public static string SteamAppsPath { get; private set; }

        static SteamInfo() {
            RegistryKey steamKey = Registry.CurrentUser.OpenSubKey(@"Software\Valve\Steam");
            object updatingObj = steamKey.GetValue("SteamPath", null);
            if (updatingObj != null) {
                SteamPath = Convert.ToString(updatingObj);
                SteamAppsPath = Path.Combine(SteamPath, "steamapps");
            }

            SteamApps = steamKey.OpenSubKey(@"Apps");
        }
    }
}