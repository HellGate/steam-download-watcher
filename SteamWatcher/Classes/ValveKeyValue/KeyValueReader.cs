﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SteamDownloadWatcher.Classes.ValveKeyValue {

    internal class KeyValueReader : StreamReader {

        private static Dictionary<char, char> sequences = new Dictionary<char, char> {
                { 'n', '\n' },
                { 'r', '\r' },
                { 't', '\t' },
                { 'v', '\v' }, // is this even used?
            };

        public KeyValueReader(Stream input) : base(input) {
        }

        /// <summary>
        /// Detect and skip whitespaces
        /// </summary>
        private void SkipWhiteSpace() {
            while (!EndOfStream) {
                if (!Char.IsWhiteSpace((char)Peek())) {
                    break;
                }

                Read();
            }
        }

        /// <summary>
        /// Detect and skip comments
        /// </summary>
        /// <returns>If a comment was found</returns>
        private bool SkipComment() {
            if (!EndOfStream) {
                char next = (char)Peek();
                // like valves parser, only 1 slash is required for a comment
                if (next == '/') {
                    // Skip line
                    ReadLine();
                    return true;
                }

                return false;
            }

            return false;
        }

        /// <summary>
        /// Reads and creates a string till a non escaped quote
        /// </summary>
        /// <returns></returns>
        private string ReadString() {
            // we are now reading a string
            var sb = new StringBuilder();
            while (!EndOfStream) {
                // check for escaped chars
                if (Peek() == '\\') {
                    // skip the backslash
                    Read();

                    char escaped = (char)Read();

                    // check if its a sequence or escaped char
                    if (sequences.TryGetValue(escaped, out char replaced))
                        sb.Append(replaced);
                    else
                        sb.Append(escaped);

                    continue;
                }

                // end of string
                if (Peek() == '"')
                    break;

                // add char to string
                sb.Append((char)Read());
            }

            return sb.ToString();
        }

        public string ReadToken(out bool quoted, out bool conditional) {
            quoted = false;
            conditional = false;

            // eating white spaces and remarks loop
            while (true) {
                SkipWhiteSpace();

                if (EndOfStream) {
                    return null;
                }

                // stop if it's not a comment; a new token starts here
                if (!SkipComment()) {
                    break;
                }
            }

            if (EndOfStream)
                return null;

            char next = (char)Peek();

            // read quoted strings specially
            if (next == '"') {
                quoted = true;

                // skip starting quote
                Read();

                // TODO: token size is limited to 1024-1
                string s = ReadString();

                // skip ending quote
                Read();

                return s;
            }

            if (next == '{' || next == '}') {
                // it's a control char, just add this one char and stop reading
                Read();
                return next.ToString();
            }

            // read in the token until we hit a whitespace or a control character
            // TODO: token size is limited to 1024-1
            bool bConditionalStart = false;
            var ret = new StringBuilder();
            while (!EndOfStream) {
                next = (char)Peek();

                // break if any control character appears in non quoted tokens
                if (next == '"' || next == '{' || next == '}')
                    break;

                if (next == '[')
                    bConditionalStart = true;

                if (next == ']' && bConditionalStart)
                    conditional = true;

                // break on whitespace
                if (Char.IsWhiteSpace(next))
                    break;

                ret.Append(next);

                Read();
            }

            return ret.ToString();
        }
    }
}