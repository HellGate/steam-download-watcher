﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SteamDownloadWatcher.Classes.ValveKeyValue {

    //bookmarks:
    //https://developer.valvesoftware.com/wiki/KeyValues
    //https://github.com/ValveSoftware/source-sdk-2013/blob/master/sp/src/tier1/KeyValues.cpp#L525, #L2092
    public class KeyValue {
        public string Key { get; set; }
        public string Value { get; set; }

        public List<KeyValue> Children { get; private set; } = new List<KeyValue>();

        public KeyValue(string key = null, string value = null) {
            Key = key;
            Value = value;
        }

        public KeyValue this[string key] {
            get {
                return Children.FirstOrDefault(child => string.Equals(key, child.Key, StringComparison.OrdinalIgnoreCase));
            }
        }

        public string AsString() {
            return Value;
        }

        public int AsInt() {
            return int.Parse(Value);
        }

        public float AsFloat() {
            return float.Parse(Value);
        }

        public long AsLong() {
            return long.Parse(Value);
        }

        public ulong AsULong() {
            return ulong.Parse(Value);
        }

        public bool AsBool() {
            return bool.Parse(Value);
        }

        public T AsEnum<T>() where T : struct {
            if (Enum.TryParse<T>(Value, out T val))
                return val;

            return default(T);
        }

        #region Load

        public static KeyValue ReadFromFile(string filename) {
            KeyValue currentKey = new KeyValue();

            using (var kvr = new KeyValueReader(File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))) {
                currentKey.LoadFromBuffer(kvr);
            }

            return currentKey;
        }

        private void LoadFromBuffer(KeyValueReader kvr) {
            do {
                string token = kvr.ReadToken(out bool quoted, out bool conditional);

                if (string.IsNullOrEmpty(token))
                    break;

                // #include, #base?

                this.Key = token;

                // get the '{'
                token = kvr.ReadToken(out quoted, out conditional);

                if (conditional) {
                    // Now get the '{'
                    token = kvr.ReadToken(out quoted, out conditional);
                }

                if (token == "{" && !quoted) {
                    // header is valid so load the file
                    this.RecursiveLoadFromBuffer(kvr);
                } else {
                    throw new Exception("LoadFromBuffer: missing {");
                }
            } while (!kvr.EndOfStream);
        }

        private void RecursiveLoadFromBuffer(KeyValueReader kvr) {
            while (true) {
                // get the key name
                string name = kvr.ReadToken(out bool quoted, out bool conditional);

                if (string.IsNullOrEmpty(name)) {
                    throw new Exception("RecursiveLoadFromBuffer: got empty keyname");
                }

                if (name == "}" && !quoted) // top level closed, stop reading
                    break;

                KeyValue dat = new KeyValue(name);
                Children.Add(dat);

                // get the value
                string value = kvr.ReadToken(out quoted, out conditional);

                if (conditional && value != null) {
                    value = kvr.ReadToken(out quoted, out conditional);
                }

                if (value == null)
                    throw new Exception("RecursiveLoadFromBuffer: got NULL key");

                if (value == "}" && !quoted)
                    throw new Exception("RecursiveLoadFromBuffer: got } in key");

                if (value == "{" && !quoted) {
                    dat.RecursiveLoadFromBuffer(kvr);
                } else {
                    if (conditional) {
                        throw new Exception("RecursiveLoadFromBuffer: got conditional between key and value");
                    }

                    dat.Value = value;
                }
            }
        }

        #endregion Load
    }
}