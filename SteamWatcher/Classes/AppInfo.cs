﻿using SteamDownloadWatcher.Classes.ValveKeyValue;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace SteamDownloadWatcher.Classes {

    public class AppInfo : INotifyPropertyChanged {

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Property Changed

        private bool _requestedName = false;

        private uint _steamId;

        public uint SteamId {
            get { return _steamId; }
            set {
                _steamId = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayName));
            }
        }

        private string _name = null;

        public string Name {
            get { return _name; }
            set {
                _name = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayName));
            }
        }

        private string _downloadSize;

        public string DownloadSize {
            get { return _downloadSize; }
            set {
                _downloadSize = value;
                OnPropertyChanged();
            }
        }

        public string DisplayName { get => Name ?? SteamId.ToString(); }

        public AppInfo(uint steamid) {
            _steamId = steamid;
            RequestAppName();
        }

        public void RequestAppName() {
            if (!_requestedName) {
                _requestedName = true;

                var worker = new BackgroundWorker();
                worker.DoWork += (se, e) => {
                    try {
                        KeyValue kv = KeyValue.ReadFromFile(Path.Combine(SteamInfo.SteamAppsPath, $"appmanifest_{_steamId}.acf"));
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.DataBind, new Action(() => {
                            Name = kv["name"].AsString();
                            long dlsize = kv["BytesToDownload"].AsLong();
                            if (dlsize == 0) {
                                // we could try again later when steam updated the file but its not that important
                                // TODO: maybe use FileSystemWatcher to watch for file changes
                                DownloadSize = "unknown";
                            } else {
                                DownloadSize = ByteSizeFormatter.BytesToString(dlsize);
                            }
                        }));
                    } catch (Exception ex) {
                        // oh well. the tool still works if this fails
                        Debug.WriteLine(ex.ToString());
                    }
                };
                worker.RunWorkerAsync();
            }
        }
    }
}