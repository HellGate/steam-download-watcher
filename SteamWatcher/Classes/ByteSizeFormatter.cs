﻿using System;

namespace SteamDownloadWatcher.Classes {

    public static class ByteSizeFormatter {
        private static readonly string[] _names = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
        private const int _stepSize = 1024;

        public static string BytesToString(long byteCount, string format = "{0:0.0} {1}") {
            if (byteCount == 0)
                return String.Format(format, byteCount, _names[0]);

            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, _stepSize)));
            double num = Math.Round(bytes / Math.Pow(_stepSize, place), 1);
            return String.Format(format, Math.Sign(byteCount) * num, _names[place]);
        }
    }
}