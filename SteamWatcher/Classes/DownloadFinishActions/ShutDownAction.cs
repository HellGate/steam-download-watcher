﻿using System.Diagnostics;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class ShutDownAction : IDownloadFinishAction {
        public string Name => "Shut Down";
        public string ToolTip => "Shuts the Computer down";

        public void Execute(string argument = "") {
            var shutdown = new ProcessStartInfo("shutdown", "/s /t 0") {
                CreateNoWindow = true,
                UseShellExecute = false
            };
            Process.Start(shutdown);
        }
    }
}