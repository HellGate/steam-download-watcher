﻿using System.Diagnostics;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class CloseSteamAction : IDownloadFinishAction {
        public string Name => "Close Steam";
        public string ToolTip => "Kills the Steam Process";

        public void Execute(string argument = "") {
            var steamProcs = Process.GetProcessesByName("Steam");
            foreach (var proc in steamProcs) {
                proc.Kill(); // silence! i kill you!
            }
        }
    }
}