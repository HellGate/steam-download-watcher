﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class HibernateAction : IDownloadFinishAction {
        public string Name => "Hibernate";
        public string ToolTip => "Puts the Computer in Hibernation. Saves RAM Data on System Drive for the duration. Make sure your Computer supports Hibernation";

        public void Execute(string argument = "") {
            NativeMethods.SetSuspendState(true, true, true);
        }
    }
}