﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public interface IDownloadFinishAction {
        string Name { get; }
        string ToolTip { get; }

        void Execute(string argument = "");
    }
}