﻿using System;
using System.Diagnostics;
using System.Windows;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class CustomAction : IDownloadFinishAction {
        public string Name => "CMD call to [Argument]";
        public string ToolTip => "Uses the Relative Path of the Application as Starting Directory";

        public void Execute(string argument) {
            try {
                // Start cmd with /c parameter so it will execute the argument string
                var procStartInfo = new ProcessStartInfo("cmd", $"/c {argument}") {
                    CreateNoWindow = true
                };

                // Create a process from the ProcessStartInfo and start it
                var proc = new Process() {
                    StartInfo = procStartInfo
                };
                proc.Start();
            } catch (Exception e) {
                MessageBox.Show($"Failed to run custom Commad:\n\n{e.Message}"); // What do i do now? nothing? D:
            }
        }
    }
}