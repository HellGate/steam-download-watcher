﻿using System.Windows;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class InfoAction : IDownloadFinishAction {
        public string Name => "Show Info";
        public string ToolTip => "Popup showing that Downloads are finished";

        public void Execute(string argument = "") {
            MessageBox.Show("All Downloads are finished", "Download finished", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}