﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class SleepAction : IDownloadFinishAction {
        public string Name => "Sleep";
        public string ToolTip => "Puts the Computer to Sleep. All Data stays on RAM for the duration.";

        public void Execute(string argument = "") {
            NativeMethods.SetSuspendState(false, true, false);
        }
    }
}